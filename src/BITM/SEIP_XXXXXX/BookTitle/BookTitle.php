<?php
namespace App\BookTitle;

use App\Message\Message;
use App\Utility\Utility;
use PDO;
use App\Model\Database as DB;

class BookTitle extends DB
{

    private $id;
    private $book_name;
    private $author_name;
    private $soft_deleted;


    public function setData($postData){

        if(array_key_exists("id",$postData)){
            $this->id = $postData["id"];
        }

        if(array_key_exists("bookName",$postData)){
            $this->book_name = $postData["bookName"];
        }

        if(array_key_exists("authorName",$postData)){
            $this->author_name = $postData["authorName"];
        }

        if(array_key_exists("soft_deleted",$postData)){
            $this->soft_deleted = $postData["soft_deleted"];
        }
    }


    public function store(){

        $dataArray = array($this->book_name,$this->author_name) ;


        $sql = "insert into book_title(book_name,author_name) VALUES (?,?)";

        $STH = $this->DBH->prepare($sql);

        $result =  $STH->execute($dataArray);


        if($result){

            Message::message("Success! :) Data Has Been Inserted!<br>")  ;
        }
        else
        {
            Message::message("Failed! :( Data Has Not Been Inserted!<br>")  ;

        }


        Utility::redirect('index.php');


    }



    public function index(){

        $sql = "select * from book_title where soft_deleted='No'";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        return $STH->fetchAll();

    }


    public function view(){

        $sql = "select * from book_title where id=".$this->id;

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        return $STH->fetch();

    }


    public function trashed(){

        $sql = "select * from book_title where soft_deleted='Yes'";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        return $STH->fetchAll();

    }




    public function update(){

        $dataArray = array($this->book_name,$this->author_name) ;


        $sql = "UPDATE  book_title SET book_name=?,author_name=? where id=".$this->id;

        $STH = $this->DBH->prepare($sql);

        $result =  $STH->execute($dataArray);


        if($result){

            Message::message("Success! :) Data Has Been Updated!<br>")  ;
        }
        else
        {
            Message::message("Failed! :( Data Has Not Been Updated!<br>")  ;

        }


        Utility::redirect('index.php');


    }




    public function trash(){

        $dataArray = array("Yes") ;


        $sql = "UPDATE  book_title SET soft_deleted=? where id=".$this->id;

        $STH = $this->DBH->prepare($sql);

        $result =  $STH->execute($dataArray);


        if($result){

            Message::message("Success! :) Data Has Been Soft Deleted!<br>")  ;
        }
        else
        {
            Message::message("Failed! :( Data Has Not Been Soft Deleted!<br>")  ;

        }


        Utility::redirect('trashed.php');


    }



    public function recover(){

        $dataArray = array("No") ;


        $sql = "UPDATE  book_title SET soft_deleted=? where id=".$this->id;

        $STH = $this->DBH->prepare($sql);

        $result =  $STH->execute($dataArray);


        if($result){

            Message::message("Success! :) Data Has Been Recovered!<br>")  ;
        }
        else
        {
            Message::message("Failed! :( Data Has Not Been Recovered!<br>")  ;

        }


        Utility::redirect('index.php');


    }



    public function delete(){

        $sql= "DELETE from book_title where id=".$this->id;

        $result = $this->DBH->exec($sql);

        if($result){

            Message::message("Success! :) Data Has Been Parmanently Deleted!<br>")  ;
        }
        else
        {
            Message::message("Failed! :( Data Has Not Been Parmanently Deleted!<br>")  ;

        }


        Utility::redirect('index.php');


    }




    public function indexPaginator($page=1,$itemsPerPage=3){

        $start = (($page-1) * $itemsPerPage);

        $sql = "SELECT * from book_title  WHERE soft_deleted = 'No' LIMIT $start,$itemsPerPage";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $arrSomeData  = $STH->fetchAll();
        return $arrSomeData;

    }





    public function trashedPaginator($page=1,$itemsPerPage=3){

        $start = (($page-1) * $itemsPerPage);

        $sql = "SELECT * from book_title  WHERE soft_deleted = 'Yes' LIMIT $start,$itemsPerPage";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $arrSomeData  = $STH->fetchAll();
        return $arrSomeData;

    }



    public function search($requestArray){
        $sql = "";
        if( isset($requestArray['byTitle']) && isset($requestArray['byAuthor']) )  $sql = "SELECT * FROM `book_title` WHERE `soft_deleted` ='No' AND (`book_name` LIKE '%".$requestArray['search']."%' OR `author_name` LIKE '%".$requestArray['search']."%')";
        if(isset($requestArray['byTitle']) && !isset($requestArray['byAuthor']) ) $sql = "SELECT * FROM `book_title` WHERE `soft_deleted` ='No' AND `book_name` LIKE '%".$requestArray['search']."%'";
        if(!isset($requestArray['byTitle']) && isset($requestArray['byAuthor']) )  $sql = "SELECT * FROM `book_title` WHERE `soft_deleted` ='No' AND `author_name` LIKE '%".$requestArray['search']."%'";

        $STH  = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $someData = $STH->fetchAll();

        return $someData;

    }// end of search()




    public function getAllKeywords()
    {
        $_allKeywords = array();
        $WordsArr = array();

        $allData = $this->index();

        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->book_name);
        }

       // $allData = $this->index();


        foreach ($allData as $oneData) {

            $eachString= strip_tags($oneData->book_name);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);

            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field block end




        // for each search field block start
        $allData = $this->index();

        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->author_name);
        }
        $allData = $this->index();

        foreach ($allData as $oneData) {

            $eachString= strip_tags($oneData->author_name);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);
            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field block end


        return array_unique($_allKeywords);


    }// get all keywords

}












