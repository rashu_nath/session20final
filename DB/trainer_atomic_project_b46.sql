-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 31, 2017 at 02:19 PM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `trainer_atomic_project_b46`
--
CREATE DATABASE IF NOT EXISTS `trainer_atomic_project_b46` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
USE `trainer_atomic_project_b46`;

-- --------------------------------------------------------

--
-- Table structure for table `book_title`
--

CREATE TABLE IF NOT EXISTS `book_title` (
`id` int(11) NOT NULL,
  `book_name` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `author_name` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `soft_deleted` varchar(11) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'No'
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `book_title`
--

INSERT INTO `book_title` (`id`, `book_name`, `author_name`, `soft_deleted`) VALUES
(1, 'Hello', 'World', 'No'),
(2, 'Moyna', 'Jorina', 'No'),
(3, 'Moyna', 'Jorina', 'Yes'),
(4, 'Moyna', 'Jorina', 'No'),
(5, 'booktitle44444444', 'authorname444444', 'No'),
(6, 'Himu', 'Humayun Ahmed', 'No'),
(7, 'Himu', 'Humayun Ahmed', 'No'),
(8, 'Himu1', 'Humayun Ahmed1', 'No'),
(9, '2222222222222', '3333333333333', 'Yes'),
(10, 'fdsgdhgfd', '22222222', 'No'),
(11, 'xvxfdg', 'Sfse', 'Yes'),
(12, 'sdfsfdsfsf', 'gjhg jhgj', 'No'),
(13, 'fdgdgfdg', 'retete654', 'No'),
(14, 'sfds', 'sdfsfe', 'Yes'),
(15, '46464', '568588', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `hobby`
--

CREATE TABLE IF NOT EXISTS `hobby` (
`id` int(11) NOT NULL,
  `name` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `hobby` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `soft_deleted` varchar(11) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'No'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `hobby`
--

INSERT INTO `hobby` (`id`, `name`, `hobby`, `soft_deleted`) VALUES
(1, 'Mr Y', 'Electronics,Gardening,Programming,Music,Facebooking,Reading', 'No'),
(2, 'jkhkjhkjhkjhkjhkjhkjhkjh', 'Electronics,Gardening,Programming', 'No');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `book_title`
--
ALTER TABLE `book_title`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hobby`
--
ALTER TABLE `hobby`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `book_title`
--
ALTER TABLE `book_title`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `hobby`
--
ALTER TABLE `hobby`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
